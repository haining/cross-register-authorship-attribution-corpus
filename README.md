# cross-register-authorship-attribution-corpus

This corpus contains writing samples of eight authors living during the Ming and Qing Dynasties who were known capable of writing in both classical Chinese and vernacular Chinese.

## Metadata

[See the metadata here](https://docs.google.com/spreadsheets/d/1sM6bDcL32C9pDNONa0Jwto0ceaYL5mf9vtPhyXqk_4I/edit?usp=sharing).

## Preprocessing

After downloading all texts, we performed the following preprocessing:

1. We checked all texts for flaws and missing parts by consulting other digital editions and print editions.
The resources consulted are listed below.
    - [Daizhige(殆知阁)](http://www.daizhige.org/)
    - [Chinese Text Project](https://ctext.org/)
    - [Guoxue Dashi(国学大师)](http://www.guoxuedashi.com/)
    - [HanDict](http://www.zdic.net/zd/bs/)
    - [Wikisource](https://zh.m.wikisource.org/wiki/Wikisource:%E9%A6%96%E9%A1%B5)
    - [Yifan Public Library](http://www.shuku.net:8082/novels/gaoshi/gaoshi.html)
    - [Google Books](https://books.google.com/)
    - [ePUBee](http://www.ferebook.com/books/)
    - [Project Gutenberg](https://www.gutenberg.org/)
    - [Millionbook](http://www.millionbook.com/), or
    - a print version.
If multiple versions exist, we choose the one which has fewer characters missing or obvious errors. If a character in a 
text lacks a Unicode code point, we used the modern variant. 
We refer to [zdic.net](https://zdic.net) and [Unicode*pedia*](https://www.unicodepedia.com/) to check whether a character
falls outside of the Chinese Unicode set (between `\u4e00` and `\u9fff`). 
We keep outside characters if they are valid Chinese characters or punctuation, deleting them otherwise. 
Some valid Chinese characters fall outside of the aforementioned Unicode range. Characters are rarely deleted.

3. We remove title, heading, table of contents, preface, postscript, and editorial comments, as well as rhymed verse 
and prose if they are not part of the main body. Blank lines, redundant spaces, and indentation marks are removed too.

4. All the texts are automatically converted into UTF-8 encoded simplified Chinese using the Python 
package 'hanziconv' (v.0.3.2).

5. Texts are then segmented into roughly 1,000-character chunks without breaking phrase-level structures. 
Modern publishers punctuate ancient Chinese texts, which originally did not have punctuation.
We leverage these delimiters introduced by editors to avoid breaking phrases when segmenting. 
We eliminate all punctuation in the next step to restore the original formatting.

6. We remove all punctuation.

7. For authors who have only one work in a register, we split the work into two parts. 
We do this to prevent severe inflation in calculating same-register accuracy. 
We refer you to see justification for this treatment in the Appendix of our paper.

After performing these steps, we organize all chunks by register under each candidate's directory with informative file names.

## Citation

[TODO]